name := "boids-par"
scalaVersion := "3.5.0"
scalacOptions ++= Seq("-deprecation", "-feature", "-language:fewerBraces", "-Xfatal-warnings")
run / fork := true
Global / cancelable := true

libraryDependencies += "com.lihaoyi" %% "os-lib" % "0.10.4"
libraryDependencies += "com.lihaoyi" %% "cask" % "0.9.4"
libraryDependencies += "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.4"
libraryDependencies += "org.scalameta" %% "munit" % "1.0.1" % Test

enablePlugins(JmhPlugin)
