package boids
import cs214.{Vector2, BoidSequence}
import scala.collection.parallel.CollectionConverters.* // access to .par methods
import boids.conversions.NumericVectors.given

def boidsWithinRadius(thisBoid: Boid, boids: BoidSequence, radius: Float): BoidSequence =
  boids.filter(b =>
    b != thisBoid &&
      b.position.distanceTo(thisBoid.position) < radius
  )

def avoidanceForce(thisBoid: Boid, boidsWithinAvoidanceRadius: BoidSequence): cs214.Vector2 =
  boidsWithinAvoidanceRadius
    .mapVector2(b => thisBoid.position - b.position)
    .filter(_.norm > 0)
    .map(diff => diff / (diff.norm * diff.norm))
    .sum

def cohesionForce(thisBoid: Boid, boidsWithinPerceptionRadius: BoidSequence): cs214.Vector2 =
  if boidsWithinPerceptionRadius.isEmpty then
    Vector2.Zero
  else
    boidsWithinPerceptionRadius.mapVector2(_.position).sum
      / boidsWithinPerceptionRadius.length.toFloat
      - thisBoid.position

def alignmentForce(thisBoid: Boid, boidsWithinPerceptionRadius: BoidSequence): cs214.Vector2 =
  if boidsWithinPerceptionRadius.isEmpty then
    Vector2.Zero
  else
    boidsWithinPerceptionRadius.mapVector2(_.velocity).sum
      / boidsWithinPerceptionRadius.length.toFloat
      - thisBoid.velocity

def containmentForce(thisBoid: Boid, limits: BoundingBox): cs214.Vector2 =
  val horizontalForce =
    if thisBoid.position.x < limits.xmin then Vector2.UnitRight
    else if thisBoid.position.x > limits.xmax then Vector2.UnitLeft
    else Vector2.Zero
  val verticalForce =
    if thisBoid.position.y < limits.ymin then Vector2.UnitDown
    else if thisBoid.position.y > limits.ymax then Vector2.UnitUp
    else Vector2.Zero
  horizontalForce + verticalForce

def totalForce(thisBoid: Boid, allBoids: BoidSequence, physics: Physics): Vector2 =
  val withinPerceptionRadius = boidsWithinRadius(thisBoid, allBoids, physics.perceptionRadius)
  val cohere = cohesionForce(thisBoid, withinPerceptionRadius)
  val align = alignmentForce(thisBoid, withinPerceptionRadius)
  val withinAvoidanceRadius = boidsWithinRadius(thisBoid, withinPerceptionRadius, physics.avoidanceRadius)
  val avoid = avoidanceForce(thisBoid, withinAvoidanceRadius)
  val contain = containmentForce(thisBoid, physics.limits)
  val total =
    avoid * physics.avoidanceWeight +
      cohere * physics.cohesionWeight +
      align * physics.alignmentWeight +
      contain * physics.containmentWeight
  total

def clampVelocity(velocity: cs214.Vector2, minimumSpeed: Float, maximumSpeed: Float): cs214.Vector2 =
  if velocity.norm < minimumSpeed then
    velocity.normalized * minimumSpeed
  else if velocity.norm > maximumSpeed then
    velocity.normalized * maximumSpeed
  else
    velocity

def tickBoid(thisBoid: Boid, allBoids: BoidSequence, physics: Physics): Boid =
  val acceleration = totalForce(thisBoid, allBoids, physics)
  Boid(
    thisBoid.position + thisBoid.velocity,
    clampVelocity(
      thisBoid.velocity + acceleration,
      physics.minimumSpeed,
      physics.maximumSpeed
    )
  )

def tickWorld(allBoids: BoidSequence, physics: Physics): BoidSequence =
  allBoids.mapBoid(boid => tickBoid(boid, allBoids, physics))

// Port this function to use Vector[Boid], but otherwise leave it untouched
// (don't add parallelism): it serves as a baseline in the benchmarks.
def tickWorldSequential(allBoids: BoidSequence, physics: Physics): BoidSequence =
  allBoids.mapBoid(boid => tickBoid(boid, allBoids, physics))
